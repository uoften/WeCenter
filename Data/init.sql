/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : jiaotu

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-08-20 22:06:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tp_activity`
-- ----------------------------
DROP TABLE IF EXISTS `tp_activity`;
CREATE TABLE `tp_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `descr` varchar(512) DEFAULT NULL,
  `istop` int(11) DEFAULT NULL,
  `pic` varchar(128) DEFAULT NULL,
  `createdate` varchar(32) DEFAULT NULL,
  `startdate` varchar(32) DEFAULT NULL,
  `enddate` varchar(32) DEFAULT NULL,
  `content` text,
  `point` int(11) DEFAULT NULL,
  `storeid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_activity
-- ----------------------------
INSERT INTO `tp_activity` VALUES ('1', '双十一店内大庆，全场8折', '活动简短说明备注信息活动简短说明备注信息', '0', 'http://www.dammit.cn/wechat/Public/Upload/Image/2015-11-14/5646ab8734a06.jpg', '2015-10-24 11:41:47', '2015-11-20 00:00:00', '2015-11-30 00:00:00', '&lt;p style=&quot;padding: 0px; margin-top: 0px; margin-bottom: 0px; list-style: none; font-size: 14px; line-height: 26px; font-family: SimSun; color: rgb(43, 43, 43); overflow: visible !important;&quot;&gt;&lt;img src=&quot;http://www.dammit.cn/wechat/Public/Upload/Image/2015-11-14/5646ab83d114f.jpg&quot;&gt;&lt;br&gt;&lt;/p&gt;&lt;p style=&quot;padding: 0px; margin-top: 0px; margin-bottom: 0px; list-style: none; font-size: 14px; line-height: 26px; font-family: SimSun; color: rgb(43, 43, 43); overflow: visible !important;&quot;&gt;【环球网综合报道】据&lt;a href=&quot;http://country.huanqiu.com/america&quot; class=&quot;linkAbout&quot; target=&quot;_blank&quot; title=&quot;美国&quot; style=&quot;padding: 0px 0px 2px; margin-right: 5px; margin-left: 5px; color: rgb(6, 52, 111); border-bottom-width: 1px; border-bottom-style: dotted; border-bottom-color: rgb(6, 52, 111);&quot;&gt;美国&lt;/a&gt;有线电视新闻网(CNN)11月14日报道，&lt;a href=&quot;http://country.huanqiu.com/france&quot; class=&quot;linkAbout&quot; target=&quot;_blank&quot; title=&quot;法国&quot; style=&quot;padding: 0px 0px 2px; margin-right: 5px; margin-left: 5px; color: rgb(6, 52, 111); border-bottom-width: 1px; border-bottom-style: dotted; border-bottom-color: rgb(6, 52, 111);&quot;&gt;法国&lt;/a&gt;巴黎恐怖袭击事件目前已经造成至少153人死亡，其中112人死在Bataclan音乐厅。&lt;/p&gt;&lt;p style=&quot;padding: 0px; margin-top: 0px; margin-bottom: 0px; list-style: none; font-size: 14px; line-height: 26px; font-family: SimSun; color: rgb(43, 43, 43); overflow: visible !important;&quot;&gt;&lt;br&gt;&lt;/p&gt;&lt;p style=&quot;padding: 0px; margin-top: 0px; margin-bottom: 0px; list-style: none; font-size: 14px; line-height: 26px; font-family: SimSun; color: rgb(43, 43, 43); overflow: visible !important;&quot;&gt;法国电视一台的消息称，参与恐怖袭击的目前已知的实施人数是五人，袭击发生地散布在10区和11区的好几个地点，起码四处，分别是11区的Bataclan音乐厅、夏荣纳街、比夏街，10区的国王泉水街。其中最严重的是在Bataclan，这个音乐厅可容纳1500人，袭击发生时正在举办音乐会。00:30左右，警方的特警队进入将暴恐分子击毙。&lt;/p&gt;', '2000', '3');
INSERT INTO `tp_activity` VALUES ('2', '新年红包大放送', '活动简短说明备注信息活动简短说明备注信息', '0', 'http://www.dammit.cn/wechat/Public/Upload/Image/2015-11-14/5646ab83d114f.jpg', '2015-11-14 11:52:51', '2015-12-09 00:00:00', '2015-12-11 00:00:00', '&lt;p style=&quot;list-style: none; margin: 23px auto 0px; padding: 0px; color: rgb(43, 43, 43); line-height: 26px; overflow: visible !important; font-family: SimSun; font-size: 14px;&quot;&gt;11月13日,中纪委网站宣布，证监会副主席姚刚因涉嫌严重违纪，正接受组织调查。这也是目前为止证监会被查的级别最高的现任官员，也是十八大以来“一行三会”系统被查的最高级别领导干部。&lt;/p&gt;&lt;p style=&quot;list-style: none; margin: 23px auto 0px; padding: 0px; color: rgb(43, 43, 43); line-height: 26px; overflow: visible !important; font-family: SimSun; font-size: 14px;&quot;&gt;加上今年9月16日被宣布接受调查的证监会主席助理张育军，不到两个月的时间，证监会已经有两位主管核心业务的高级官员接连落马。&lt;/p&gt;&lt;p style=&quot;list-style: none; margin: 23px auto 0px; padding: 0px; color: rgb(43, 43, 43); line-height: 26px; overflow: visible !important; font-family: SimSun; font-size: 14px;&quot;&gt;事实上，从2014年下半年开始，市场上不断出现姚刚被调查的传闻。2014年底，创业板发行监管部副主任李量被中纪委调查，李量此前则在证监会发行监管部任职，参与创业板发行法规规则制定及发行审核工作，正是姚刚分管的领域。&lt;/p&gt;&lt;p style=&quot;list-style: none; margin: 23px auto 0px; padding: 0px; color: rgb(43, 43, 43); line-height: 26px; overflow: visible !important; font-family: SimSun; font-size: 14px;&quot;&gt;据市场人士透露，李量在狱中将自己知道的事情几乎无保留的交待，很可能牵涉了不少人。而姚刚被调查的传闻，更是在李量被调查后时而涌现。&lt;/p&gt;&lt;p style=&quot;list-style: none; margin: 23px auto 0px; padding: 0px; color: rgb(43, 43, 43); line-height: 26px; overflow: visible !important; font-family: SimSun; font-size: 14px;&quot;&gt;据悉，2014年，组织部门曾欲将姚刚调任天津市任副市长一职，但最终未能成行，而是由原银监会副主席阎庆民调任天津市副市长。&lt;/p&gt;&lt;p style=&quot;list-style: none; margin: 23px auto 0px; padding: 0px; color: rgb(43, 43, 43); line-height: 26px; overflow: visible !important; font-family: SimSun; font-size: 14px;&quot;&gt;在一周前的11月3日，市场盛传“证监会副主席姚刚被带走调查。”但当天晚些时间，与证监会关系密切的媒体发布新闻稿称，“第四届风险管理与农业发展论坛将召开姚刚出席”，算是间接辟谣。&lt;/p&gt;', '1000', '-1');
INSERT INTO `tp_activity` VALUES ('3', '双十一店内大庆，全场8折', '', '0', '', '2015-11-30 11:52:10', '2015-12-07 00:00:00', '2015-12-20 00:00:00', '&lt;p style=&quot;list-style: none; padding: 0px; color: rgb(43, 43, 43); line-height: 26px; overflow: visible !important; font-family: SimSun; font-size: 14px; margin-top: 0px; margin-bottom: 0px;&quot;&gt;&lt;img src=&quot;http://www.dammit.cn/wechat/Public/Upload/Image/2015-11-14/5646ab83d114f.jpg&quot;&gt;&lt;br&gt;&lt;/p&gt;&lt;p style=&quot;list-style: none; padding: 0px; color: rgb(43, 43, 43); line-height: 26px; overflow: visible !important; font-family: SimSun; font-size: 14px; margin-top: 0px; margin-bottom: 0px;&quot;&gt;【环球网综合报道】据&lt;a title=&quot;美国&quot; class=&quot;linkAbout&quot; style=&quot;padding: 0px 0px 2px; color: rgb(6, 52, 111); margin-right: 5px; margin-left: 5px; border-bottom-color: rgb(6, 52, 111); border-bottom-width: 1px; border-bottom-style: dotted;&quot; href=&quot;http://country.huanqiu.com/america&quot; target=&quot;_blank&quot;&gt;美国&lt;/a&gt;有线电视新闻网(CNN)11月14日报道，&lt;a title=&quot;法国&quot; class=&quot;linkAbout&quot; style=&quot;padding: 0px 0px 2px; color: rgb(6, 52, 111); margin-right: 5px; margin-left: 5px; border-bottom-color: rgb(6, 52, 111); border-bottom-width: 1px; border-bottom-style: dotted;&quot; href=&quot;http://country.huanqiu.com/france&quot; target=&quot;_blank&quot;&gt;法国&lt;/a&gt;巴黎恐怖袭击事件目前已经造成至少153人死亡，其中112人死在Bataclan音乐厅。&lt;/p&gt;&lt;p style=&quot;list-style: none; padding: 0px; color: rgb(43, 43, 43); line-height: 26px; overflow: visible !important; font-family: SimSun; font-size: 14px; margin-top: 0px; margin-bottom: 0px;&quot;&gt;&lt;br&gt;&lt;/p&gt;&lt;p style=&quot;list-style: none; padding: 0px; color: rgb(43, 43, 43); line-height: 26px; overflow: visible !important; font-family: SimSun; font-size: 14px; margin-top: 0px; margin-bottom: 0px;&quot;&gt;法国电视一台的消息称，参与恐怖袭击的目前已知的实施人数是五人，袭击发生地散布在10区和11区的好几个地点，起码四处，分别是11区的Bataclan音乐厅、夏荣纳街、比夏街，10区的国王泉水街。其中最严重的是在Bataclan，这个音乐厅可容纳1500人，袭击发生时正在举办音乐会。00:30左右，警方的特警队进入将暴恐分子击毙。&lt;/p&gt;', '10000', '-1');
INSERT INTO `tp_activity` VALUES ('4', '双十二狂欢', '', '0', '全场九折，会员折上折', '2015-12-11 10:56:05', '2015-12-14 00:00:00', '2016-05-16 00:00:00', '&lt;p&gt;&lt;strong&gt;&lt;/strong&gt;&lt;br&gt;&lt;/p&gt;', '5000', '-1');
INSERT INTO `tp_activity` VALUES ('5', '元旦有买有送', '', '0', '', '2015-12-11 11:01:04', '2015-12-11 00:00:00', '2016-01-10 00:00:00', '&lt;p&gt;一次&lt;font style=&quot;background-color: rgb(255, 255, 255);&quot;&gt;消费满2900送神秘礼物一件。&lt;/font&gt;&lt;br&gt;&lt;/p&gt;', '5000', '-1');

-- ----------------------------
-- Table structure for `tp_admin`
-- ----------------------------
DROP TABLE IF EXISTS `tp_admin`;
CREATE TABLE `tp_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `password` varchar(512) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `birthday` varchar(32) DEFAULT NULL,
  `realname` varchar(32) DEFAULT NULL,
  `storeid` int(11) DEFAULT NULL,
  `groupid` int(11) DEFAULT NULL,
  `createdate` varchar(32) DEFAULT NULL,
  `lastlogin` varchar(32) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `enabled` int(11) DEFAULT NULL,
  `descr` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_admin
-- ----------------------------
INSERT INTO `tp_admin` VALUES ('1', 'admin', 'admin', '13540312451', 'sleepsleepsleep@foxmail.com', '1', '1987-06-27', '超级管理员', null, '1', '2015-10-17 11:56:58', '2016-08-20 19:41:04', '0', '1', '彪悍的人生不需要解释');
INSERT INTO `tp_admin` VALUES ('2', 'jiao', 'jiaotu', '13098878628', 'sleepsleepsleep@foxmail.com', '1', '1987-06-27', '蛟', '6', '2', '2015-10-17 11:56:58', '2015-12-08 09:17:10', '0', '1', '描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息');
INSERT INTO `tp_admin` VALUES ('3', 'kangming1', 'kangming1', '13540312452', 'sleepsleepsleep@foxmail.com', '1', '1987-06-27', '店长乙', '2', '2', '2015-10-17 11:56:58', '2015-11-17 14:09:06', '0', '1', '描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息');
INSERT INTO `tp_admin` VALUES ('4', 'kangming2', 'kangming2', '13540312451', 'sleepsleepsleep@foxmail.com', '1', '1987-06-27', '店长丙', '3', '2', '2015-10-17 11:56:58', '2015-11-14 14:52:52', '0', '1', '描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息');
INSERT INTO `tp_admin` VALUES ('5', 'kangming3', 'kangming3', '13540312451', 'sleepsleepsleep@foxmail.com', '0', '1987-06-27', '店长丁', '4', '2', '2015-10-17 11:56:58', '2015-11-14 14:51:45', '0', '1', '描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息');
INSERT INTO `tp_admin` VALUES ('6', 'kangming4', 'kangming4', '13540312451', 'sleepsleepsleep@foxmail.com', '0', '1987-06-27', '店长戊', '5', '2', '2015-10-17 11:56:58', '2015-10-13 11:56:48', '0', '1', '描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息描述信息');

-- ----------------------------
-- Table structure for `tp_admin_group`
-- ----------------------------
DROP TABLE IF EXISTS `tp_admin_group`;
CREATE TABLE `tp_admin_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  `descr` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_admin_group
-- ----------------------------

-- ----------------------------
-- Table structure for `tp_help`
-- ----------------------------
DROP TABLE IF EXISTS `tp_help`;
CREATE TABLE `tp_help` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `content` varchar(512) DEFAULT NULL,
  `createdate` varchar(32) DEFAULT NULL,
  `enabled` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_help
-- ----------------------------

-- ----------------------------
-- Table structure for `tp_level`
-- ----------------------------
DROP TABLE IF EXISTS `tp_level`;
CREATE TABLE `tp_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  `pic` varchar(256) DEFAULT NULL,
  `descr` varchar(512) DEFAULT NULL,
  `point_begin` int(11) DEFAULT NULL,
  `point_end` int(11) DEFAULT NULL,
  `createtime` varchar(32) DEFAULT NULL,
  `updatetime` varchar(32) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_level
-- ----------------------------
INSERT INTO `tp_level` VALUES ('1', '白金卡', 'http://www.jiaotumaoyi.com/wechat/Public/Upload/Image/2015-11-14/5646ab75231d3.png', '全场商品8.5折优惠', '10000', '50000', '2015-10-18 18:05:38', '2015-12-07 11:01:35', '0');
INSERT INTO `tp_level` VALUES ('2', '银卡会员', 'http://www.jiaotumaoyi.com/wechat/Public/Upload/Image/2015-11-14/5646ab6c19bcf.png', '全场商品9.5折优惠', '1000', '5000', '2015-10-18 18:07:33', '2015-12-14 19:40:36', '0');
INSERT INTO `tp_level` VALUES ('3', '金卡会员', 'http://www.jiaotumaoyi.com/wechat/Public/Upload/Image/2015-11-14/5646ab712a265.png', '全场商品9折优惠', '5000', '10000', '2015-11-14 11:38:28', '2015-12-07 11:01:36', '0');
INSERT INTO `tp_level` VALUES ('4', '普通会员', 'http://www.jiaotumaoyi.com/wechat/Public/Upload/Image/2015-11-14/5646ab6c19bcf.png', '普通会员', '0', '1000', '2015-12-14 19:40:12', '2015-12-14 19:40:12', '0');

-- ----------------------------
-- Table structure for `tp_store`
-- ----------------------------
DROP TABLE IF EXISTS `tp_store`;
CREATE TABLE `tp_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  `address` varchar(128) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `adminid` int(11) DEFAULT NULL,
  `descr` varchar(512) DEFAULT NULL,
  `createtime` varchar(32) DEFAULT NULL,
  `updatetime` varchar(32) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_store
-- ----------------------------
INSERT INTO `tp_store` VALUES ('1', '四川省成都市天府二街旗舰店', '四川省成都市天府二街196号', '13540312451', '353066897@qq.com', '-1', '描描述信息描述信息描述信息描述信息描述信息描述信息描述信息述信息', '2015-10-17 14:16:20', '2015-10-18 11:30:47', '0');
INSERT INTO `tp_store` VALUES ('2', '四川省成都市天府三街旗舰店', '四川省成都市天府三街256号', '13540312451', '353066897@qq.com', '3', '描描述信息描述信息描述信息描述信息描述信息描述信息描述信息述信息', '2015-10-17 14:16:20', '2015-11-02 11:17:53', '0');
INSERT INTO `tp_store` VALUES ('3', '四川省成都市天府四街旗舰店', '四川省成都市天府四街256号', '13540312451', '353066897@qq.com', '4', '描描述信息描述信息描述信息描述信息描述信息描述信息描述信息述信息', '2015-10-17 14:16:20', '2015-10-17 14:16:20', '0');
INSERT INTO `tp_store` VALUES ('4', '四川省成都市天府五街旗舰店', '四川省成都市天府五街256号', '13540312451', '353066897@qq.com', '5', '描描述信息描述信息描述信息描述信息描述信息描述信息描述信息述信息', '2015-10-17 14:16:20', '2015-10-17 14:16:20', '0');
INSERT INTO `tp_store` VALUES ('5', '四川省成都市天府六街旗舰店', '四川省成都市天府五街256号', '13540312451', '353066897@qq.com', '6', '描描述信息描述信息描述信息描述信息描述信息描述信息描述信息述信息', '2015-10-17 14:16:20', '2015-10-17 14:16:20', '0');
INSERT INTO `tp_store` VALUES ('6', '曼陀莉歌盘锦店', '番禺大石 ', '020-62319432', null, '2', '', '2015-11-30 11:47:00', '2015-11-30 12:31:52', '0');

-- ----------------------------
-- Table structure for `tp_user`
-- ----------------------------
DROP TABLE IF EXISTS `tp_user`;
CREATE TABLE `tp_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(32) DEFAULT NULL,
  `pointmax` int(11) DEFAULT NULL,
  `point` int(11) DEFAULT NULL,
  `avatar` varchar(128) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `createdate` varchar(32) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `lastlogin` varchar(32) DEFAULT NULL,
  `enabled` int(11) DEFAULT '1',
  `useragent` varchar(512) DEFAULT NULL,
  `descr` varchar(512) DEFAULT NULL,
  `openid` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_user
-- ----------------------------
INSERT INTO `tp_user` VALUES ('21', '13540312451', '28030', '5230', '', '', '2015-11-22 13:41:24', '3', '2015-11-22 13:41:24', '1', '13540312451', null, 'oqVJowJcAgovWLgjZxJwZKj-PRD8');
INSERT INTO `tp_user` VALUES ('22', '15881029673', '21200', '3900', '', '', '2015-11-24 11:43:04', '1', '2015-11-24 11:43:04', '1', '15881029673', '', 'oqVJowOQpuaz8UM_hi0kyODikbsw');
INSERT INTO `tp_user` VALUES ('23', '13458699876', '12040', '840', '', '', '2015-11-24 14:22:02', '4', '2015-11-24 14:22:02', '1', '13458699876', '', 'oqVJowH86OhMPV6NNEjOSKoHPBbI');
INSERT INTO `tp_user` VALUES ('24', '15134572003', '20355', '11200', '', '', '2015-11-28 12:29:05', '1', '2015-11-28 12:29:05', '1', '15134572003', null, 'oqVJowJ59h5UAVRh-edL1p_OshLE');
INSERT INTO `tp_user` VALUES ('25', '13098878628', '5321', '521', '', '', '2015-11-30 10:11:22', '3', '2015-11-30 10:11:22', '1', '13098878628', null, 'oqVJowPHS9pbfKDkJWT67GJ3E6c4');
INSERT INTO `tp_user` VALUES ('26', '13763052264', '5820', '920', '', '', '2015-11-30 11:57:06', '3', '2015-11-30 11:57:06', '1', '13763052264', null, 'oqVJowIqssamiM_74zqBzN9aLJD8');

-- ----------------------------
-- Table structure for `tp_user_point`
-- ----------------------------
DROP TABLE IF EXISTS `tp_user_point`;
CREATE TABLE `tp_user_point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `type` varchar(32) DEFAULT NULL,
  `descr` varchar(512) DEFAULT NULL,
  `manager` int(11) DEFAULT NULL,
  `attach` varchar(512) DEFAULT NULL,
  `point` int(11) DEFAULT '0',
  `createdate` varchar(32) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_user_point
-- ----------------------------
INSERT INTO `tp_user_point` VALUES ('63', '21', '注册赠送', '', null, null, '800', '2015-11-22 13:41:24', '1');
INSERT INTO `tp_user_point` VALUES ('64', '21', '签到赠送', '', null, null, '10', '2015-11-22 13:41:28', '1');
INSERT INTO `tp_user_point` VALUES ('65', '21', '商品购买', '11', '2', 'Upload/Attach/2015-11-22/5651568122d21.jpg', '200', '2015-11-22 13:45:37', '1');
INSERT INTO `tp_user_point` VALUES ('66', '21', '商品购买', '5000', '2', 'Upload/Attach/2015-11-22/565158acddbda.jpg', '5000', '2015-11-22 13:54:52', '1');
INSERT INTO `tp_user_point` VALUES ('67', '21', '积分兑换', 'bad', '2', 'Upload/Attach/2015-11-22/565158e7ecffe.jpg', '-5500', '2015-11-22 13:55:51', '1');
INSERT INTO `tp_user_point` VALUES ('68', '22', '注册赠送', '', null, null, '800', '2015-11-24 11:43:04', '1');
INSERT INTO `tp_user_point` VALUES ('69', '22', '签到赠送', '', null, null, '10', '2015-11-24 11:43:10', '1');
INSERT INTO `tp_user_point` VALUES ('70', '23', '注册赠送', '', null, null, '800', '2015-11-24 14:22:02', '1');
INSERT INTO `tp_user_point` VALUES ('71', '23', '签到赠送', '', null, null, '10', '2015-11-24 14:22:12', '1');
INSERT INTO `tp_user_point` VALUES ('72', '23', '签到赠送', '', null, null, '10', '2015-11-28 12:26:05', '1');
INSERT INTO `tp_user_point` VALUES ('73', '24', '注册赠送', '', null, null, '800', '2015-11-28 12:29:05', '1');
INSERT INTO `tp_user_point` VALUES ('74', '24', '签到赠送', '', null, null, '10', '2015-11-28 12:29:14', '1');
INSERT INTO `tp_user_point` VALUES ('75', '23', '商品购买', '商品购买', '1', 'Upload/Attach/2015-11-30/565ba7c072bf0.jpg', '5000', '2015-11-30 09:34:56', '1');
INSERT INTO `tp_user_point` VALUES ('76', '24', '商品购买', '积分增加测试', '1', 'Upload/Attach/2015-11-30/565ba800768f9.jpg', '12345', '2015-11-30 09:36:00', '1');
INSERT INTO `tp_user_point` VALUES ('77', '21', '商品购买', '积分测试', '1', 'Upload/Attach/2015-11-30/565ba8684127b.jpg', '5500', '2015-11-30 09:37:44', '1');
INSERT INTO `tp_user_point` VALUES ('78', '21', '商品购买', '积分测试\r\n', '1', 'Upload/Attach/2015-11-30/565ba99f674d5.jpg', '4900', '2015-11-30 09:42:55', '1');
INSERT INTO `tp_user_point` VALUES ('79', '23', '签到赠送', '', null, null, '10', '2015-11-30 09:45:15', '1');
INSERT INTO `tp_user_point` VALUES ('80', '25', '注册赠送', '', null, null, '800', '2015-11-30 10:11:22', '1');
INSERT INTO `tp_user_point` VALUES ('81', '25', '签到赠送', '', null, null, '10', '2015-11-30 10:11:52', '1');
INSERT INTO `tp_user_point` VALUES ('82', '25', '商品购买', '', '1', 'Upload/Attach/2015-11-30/565bbc2217318.jpg', '100', '2015-11-30 11:01:54', '1');
INSERT INTO `tp_user_point` VALUES ('83', '24', '积分兑换', '', '1', 'Upload/Attach/2015-11-30/565bbd5bb3989.jpg', '-500', '2015-11-30 11:07:07', '1');
INSERT INTO `tp_user_point` VALUES ('84', '26', '注册赠送', '', null, null, '800', '2015-11-30 11:57:06', '1');
INSERT INTO `tp_user_point` VALUES ('85', '26', '签到赠送', '', null, null, '10', '2015-11-30 11:58:28', '1');
INSERT INTO `tp_user_point` VALUES ('86', '22', '商品购买', '', '1', 'Upload/Attach/2015-11-30/565bc9b50f906.jpg', '300', '2015-11-30 11:59:49', '1');
INSERT INTO `tp_user_point` VALUES ('87', '23', '积分兑换', '', '1', 'Upload/Attach/2015-11-30/565bca2c5bdba.jpg', '70', '2015-11-30 12:01:48', '1');
INSERT INTO `tp_user_point` VALUES ('88', '23', '积分兑换', '', '2', 'Upload/Attach/2015-11-30/565bd19117318.jpg', '100', '2015-11-30 12:33:21', '1');
INSERT INTO `tp_user_point` VALUES ('89', '21', '积分兑换', '', '2', 'Upload/Attach/2015-11-30/565bd2ae637cc.jpg', '-1000', '2015-11-30 12:38:06', '1');
INSERT INTO `tp_user_point` VALUES ('90', '22', '商品购买', '', '2', 'Upload/Attach/2015-11-30/565bd3b522a33.jpg', '90', '2015-11-30 12:42:29', '1');
INSERT INTO `tp_user_point` VALUES ('91', '21', '商品购买', '', '2', 'Upload/Attach/2015-11-30/565bd44d004e2.jpg', '90', '2015-11-30 12:45:01', '1');
INSERT INTO `tp_user_point` VALUES ('92', '24', '积分兑换', '', '1', 'Upload/Attach/2015-11-30/565bd4d35fac3.jpg', '-655', '2015-11-30 12:47:15', '1');
INSERT INTO `tp_user_point` VALUES ('93', '25', '商品购买', '', '1', 'Upload/Attach/2015-11-30/565bd5268d72f.jpg', '200', '2015-11-30 12:48:38', '1');
INSERT INTO `tp_user_point` VALUES ('94', '24', '积分兑换', '', '1', 'Upload/Attach/2015-11-30/565bdbda72bf0.jpg', '-3000', '2015-11-30 13:17:14', '1');
INSERT INTO `tp_user_point` VALUES ('95', '23', '签到赠送', '', null, null, '10', '2015-12-01 10:31:38', '1');
INSERT INTO `tp_user_point` VALUES ('96', '21', '签到赠送', '', null, null, '10', '2015-12-01 21:42:24', '1');
INSERT INTO `tp_user_point` VALUES ('97', '24', '积分兑换', '', '1', 'Upload/Attach/2015-12-01/565da826ca7bf.jpg', '-1000', '2015-12-01 22:01:10', '1');
INSERT INTO `tp_user_point` VALUES ('98', '26', '签到赠送', '', null, null, '10', '2015-12-01 22:01:43', '1');
INSERT INTO `tp_user_point` VALUES ('99', '24', '商品购买', '', '1', 'Upload/Attach/2015-12-01/565da863ce4c8.jpg', '200', '2015-12-01 22:02:11', '1');
INSERT INTO `tp_user_point` VALUES ('100', '24', '积分兑换', '', '1', 'Upload/Attach/2015-12-01/565da88417318.jpg', '-1000', '2015-12-01 22:02:44', '1');
INSERT INTO `tp_user_point` VALUES ('101', '25', '商品购买', '', '1', 'Upload/Attach/2015-12-01/565da8e35bdba.jpg', '100', '2015-12-01 22:04:19', '1');
INSERT INTO `tp_user_point` VALUES ('102', '25', '积分兑换', '', '1', 'Upload/Attach/2015-12-01/565da91f5069f.jpg', '-500', '2015-12-01 22:05:19', '1');
INSERT INTO `tp_user_point` VALUES ('103', '25', '商品购买', '', '2', 'Upload/Attach/2015-12-02/565e6aade15f5.jpg', '100', '2015-12-02 11:51:09', '-1');
INSERT INTO `tp_user_point` VALUES ('104', '26', '商品购买', null, '1', 'Upload/Attach/2015-12-02/565e8e901360f.jpg', '-500', '2015-12-02 14:24:16', '1');
INSERT INTO `tp_user_point` VALUES ('105', '26', '商品购买', '', '1', 'Upload/Attach/2015-12-02/565e8ea34127b.jpg|Upload/Attach/2015-12-02/565e8ea344f84.jpg', '-500', '2015-12-02 14:24:35', '-1');
INSERT INTO `tp_user_point` VALUES ('106', '26', '商品购买', '', '1', 'Upload/Attach/2015-12-02/565e8fb45fac3.jpg', '5000', '2015-12-02 14:29:08', '1');
INSERT INTO `tp_user_point` VALUES ('107', '21', '签到赠送', '', null, null, '10', '2015-12-04 10:55:54', '1');
INSERT INTO `tp_user_point` VALUES ('108', '23', '商品购买', '', '2', 'Upload/Attach/2015-12-07/5664f345040f1.png', '6000', '2015-12-07 10:47:33', '1');
INSERT INTO `tp_user_point` VALUES ('109', '21', '商品购买', '', '2', 'Upload/Attach/2015-12-07/5664f35f3d478.png', '7000', '2015-12-07 10:47:59', '1');
INSERT INTO `tp_user_point` VALUES ('110', '21', '商品购买', '', '1', 'Upload/Attach/2015-12-07/5664f3cfd9ae9.png', '-15000', '2015-12-07 10:49:51', '1');
INSERT INTO `tp_user_point` VALUES ('111', '23', '商品购买', '', '1', 'Upload/Attach/2015-12-07/5664f43e767ff.png', '-10000', '2015-12-07 10:51:42', '1');
INSERT INTO `tp_user_point` VALUES ('112', '22', '商品购买', '', '1', 'Upload/Attach/2015-12-07/5664f472542ae.png', '10000', '2015-12-07 10:52:34', '1');
INSERT INTO `tp_user_point` VALUES ('113', '25', '商品购买', '测试', '1', 'Upload/Attach/2015-12-07/5664f6d4abe7d.jpg', '1111', '2015-12-07 11:02:44', '1');
INSERT INTO `tp_user_point` VALUES ('114', '26', '商品购买', '测试', '1', 'Upload/Attach/2015-12-07/5664f6f27e211.jpg', '-400', '2015-12-07 11:03:14', '1');
INSERT INTO `tp_user_point` VALUES ('115', '24', '积分兑换', '测试', '1', 'Upload/Attach/2015-12-07/5664f71bb388f.jpg', '-3000', '2015-12-07 11:03:55', '1');
INSERT INTO `tp_user_point` VALUES ('116', '22', '积分兑换', '测试', '1', 'Upload/Attach/2015-12-07/5664f737c69bc.jpg', '-10000', '2015-12-07 11:04:23', '1');
INSERT INTO `tp_user_point` VALUES ('117', '22', '商品购买', '', '1', 'Upload/Attach/2015-12-07/5664f7f581f1a.png', '10000', '2015-12-07 11:07:33', '1');
INSERT INTO `tp_user_point` VALUES ('118', '22', '商品购买', '', '1', 'Upload/Attach/2015-12-07/5664f81a3976f.png', '-7300', '2015-12-07 11:08:10', '1');
INSERT INTO `tp_user_point` VALUES ('119', '26', '积分兑换', '', '1', 'Upload/Attach/2015-12-07/56652dcaa8174.jpg', '-4000', '2015-12-07 14:57:14', '1');
INSERT INTO `tp_user_point` VALUES ('120', '25', '积分兑换', '测试', '1', 'Upload/Attach/2015-12-07/56652e043d478.jpg', '-1800', '2015-12-07 14:58:12', '1');
INSERT INTO `tp_user_point` VALUES ('121', '23', '积分兑换', '测试', '1', 'Upload/Attach/2015-12-07/56652e1f8992c.jpg', '-1200', '2015-12-07 14:58:39', '1');
INSERT INTO `tp_user_point` VALUES ('122', '21', '商品购买', '测试', '1', 'Upload/Attach/2015-12-07/56652e3826642.jpg', '-1300', '2015-12-07 14:59:04', '1');
INSERT INTO `tp_user_point` VALUES ('123', '24', '商品购买', '测试', '1', 'Upload/Attach/2015-12-07/56652e52abe7d.jpg', '7000', '2015-12-07 14:59:30', '1');
INSERT INTO `tp_user_point` VALUES ('124', '21', '签到赠送', '', null, null, '10', '2015-12-10 10:55:44', '1');
INSERT INTO `tp_user_point` VALUES ('125', '23', '签到赠送', '', null, null, '10', '2015-12-10 11:00:04', '1');
INSERT INTO `tp_user_point` VALUES ('126', '23', '签到赠送', '', null, null, '10', '2015-12-11 09:56:39', '1');
INSERT INTO `tp_user_point` VALUES ('127', '23', '签到赠送', '', null, null, '10', '2015-12-14 09:18:11', '1');
INSERT INTO `tp_user_point` VALUES ('128', '21', '商品购买', '测试', '1', 'Upload/Attach/2015-12-14/566e7fafce0e0.jpg', '4500', '2015-12-14 16:37:03', '1');
INSERT INTO `tp_user_point` VALUES ('129', '25', '商品购买', '11', '1', 'Upload/Attach/2015-12-14/566eb52a40f09.jpg', '3000', '2015-12-14 20:25:14', '1');
INSERT INTO `tp_user_point` VALUES ('130', '25', '积分兑换', '22', '1', 'Upload/Attach/2015-12-14/566eb55063da0.jpg', '-2500', '2015-12-14 20:25:52', '1');

-- ----------------------------
-- Table structure for `tp_user_point_audit`
-- ----------------------------
DROP TABLE IF EXISTS `tp_user_point_audit`;
CREATE TABLE `tp_user_point_audit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pointid` int(11) DEFAULT NULL,
  `authmanager` int(11) DEFAULT NULL,
  `authtime` varchar(32) DEFAULT NULL,
  `authdescr` varchar(512) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_user_point_audit
-- ----------------------------
INSERT INTO `tp_user_point_audit` VALUES ('26', '65', '1', '2015-11-22 13:46:03', 'ok', '1');
INSERT INTO `tp_user_point_audit` VALUES ('27', '66', '1', '2015-11-22 13:55:13', 'ok', '1');
INSERT INTO `tp_user_point_audit` VALUES ('28', '67', '1', '2015-11-22 13:56:13', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('29', '75', '1', '2015-11-30 09:35:21', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('30', '76', '1', '2015-11-30 09:36:10', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('31', '77', '1', '2015-11-30 09:42:17', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('32', '78', '1', '2015-11-30 09:43:06', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('33', '82', '1', '2015-11-30 11:03:04', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('34', '83', '1', '2015-11-30 11:10:46', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('35', '86', '1', '2015-11-30 12:00:04', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('36', '87', '1', '2015-11-30 12:01:54', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('37', '88', '1', '2015-11-30 12:41:04', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('38', '89', '1', '2015-11-30 12:41:11', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('39', '90', '1', '2015-11-30 12:42:42', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('40', '91', '1', '2015-11-30 12:45:12', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('41', '92', '1', '2015-11-30 12:47:36', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('42', '93', '1', '2015-11-30 12:48:51', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('43', '94', '1', '2015-11-30 13:17:23', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('44', '97', '1', '2015-12-01 22:01:30', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('45', '99', '1', '2015-12-01 22:02:22', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('46', '100', '1', '2015-12-01 22:02:57', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('47', '101', '1', '2015-12-01 22:04:27', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('48', '102', '1', '2015-12-01 22:05:32', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('49', '103', '1', '2015-12-02 11:51:32', '', '-1');
INSERT INTO `tp_user_point_audit` VALUES ('50', '105', '1', '2015-12-02 14:27:43', '', '-1');
INSERT INTO `tp_user_point_audit` VALUES ('51', '104', '1', '2015-12-02 14:27:53', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('52', '106', '1', '2015-12-02 14:29:21', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('53', '108', '1', '2015-12-07 10:48:29', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('54', '109', '1', '2015-12-07 10:48:37', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('55', '110', '1', '2015-12-07 10:50:03', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('56', '111', '1', '2015-12-07 10:51:57', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('57', '112', '1', '2015-12-07 10:52:43', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('58', '113', '1', '2015-12-07 11:04:38', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('59', '114', '1', '2015-12-07 11:04:46', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('60', '115', '1', '2015-12-07 11:04:51', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('61', '116', '1', '2015-12-07 11:04:57', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('62', '117', '1', '2015-12-07 11:07:45', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('63', '118', '1', '2015-12-07 11:08:20', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('64', '119', '1', '2015-12-07 14:57:28', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('65', '120', '1', '2015-12-07 14:59:43', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('66', '121', '1', '2015-12-07 14:59:48', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('67', '122', '1', '2015-12-07 14:59:53', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('68', '123', '1', '2015-12-07 14:59:59', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('69', '128', '1', '2015-12-14 16:37:12', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('70', '129', '1', '2015-12-14 20:25:27', '', '1');
INSERT INTO `tp_user_point_audit` VALUES ('71', '130', '1', '2015-12-14 20:25:59', '', '1');
